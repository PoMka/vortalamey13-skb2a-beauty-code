import express from 'express';

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/task2a', (req, res) => {
  const a = !isNaN(req.query.a) && +req.query.a;
  const b = !isNaN(req.query.b) && +req.query.b;

  res.send(`${a + b}`);
});

app.get('/task2b', (req, res) => {
  const fullname = req.query.fullname.toUpperCase() || '';
  let initials = fullname ? fullname.split(' ').filter(el => el) : 0;
  let shortname = 'Invalid fullname';
  let lastname;

  if (initials && initials.length <= 3 && !/[\d_/]+/.test(fullname)) {
    lastname = initials.pop();
    lastname = lastname[0] + lastname.slice(1).toLowerCase();
    initials = initials.map(el => `${el[0]}.`);
    shortname = [lastname, ...initials].join(' ');
  }

  res.send(shortname);
});

app.listen(3000, () => {
  console.log('On http://localhost:3000/');
});
